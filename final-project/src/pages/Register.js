import React, { useContext, useState } from "react"
import {UserContext} from "../context/UserContext"
import Axios from "axios"

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {Button} from '@material-ui/core'


const Register = () =>{
    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({name: "", email: "" , password: ""})

    const useStyles = makeStyles((theme) => ({
        margin: {
          marginLeft:'25%',
          marginTop: '10px'
        },
        textField: {
          width: '25ch',
        },
      }));
    
    const classes = useStyles();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleSubmitFormUser = (event) =>{
        event.preventDefault()
        Axios.post("https://backendexample.sanbersy.com/api/register", {
        name: input.name, 
        email: input.email, 
        password: input.password
        }).then(
        (res)=>{
            var user = res.data.user
            var token = res.data.token
            var currentUser = {name: user.name, email: user.email, token }
            setUser(currentUser)
            localStorage.setItem("user", JSON.stringify(currentUser))
            alert("Register Success")
        }
        ).catch((err)=>{
            alert(err)
        })
    }

    const handleChangeFormUser = (event) =>{
        let value = event.target.value
        let name = event.target.name
        switch (name){
        case "name":{
            setInput({...input, name: value})
            break;
        }
        case "email":{
            setInput({...input, email: value})
            break;
        }
        case "password":{
            setInput({...input, password: value})
            break;
        }
        default:{break;}
        }
    }
    
    const clickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    
    const mouseDownPassword = (event) => {
        event.preventDefault();
    };

        return(
            <>
                <div style={{margin: "100px auto 0 auto", width: "25%", padding: "50px"}}>
                    <form onSubmit={handleSubmitFormUser}>
                        <label style={{fontSize:'8pt',marginLeft:'80px'}}>Masukkan Nama Anda: </label><br/>
                        <TextField className={clsx(classes.margin, classes.textField)} required name='name' id="outlined-required" label="Name" onChange={handleChangeFormUser} style={{marginBottom:'20px'}} value={input.name} variant="outlined"/>
                        <br/>
                        <label style={{fontSize:'8pt',marginLeft:'80px'}}>Masukkan Email Anda: </label><br/>
                        <TextField className={clsx(classes.margin, classes.textField)} required name='email' id="outlined-required" label="Email" onChange={handleChangeFormUser} style={{marginBottom:'20px'}} value={input.email}  variant="outlined"/>
                        <br/>
                        <label style={{fontSize:'8pt',marginLeft:'80px'}}>Masukkan Password Anda:</label><br/>
                        <FormControl className={clsx(classes.margin, classes.textField)} style={{marginBottom:'20px'}} variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-password">Password*</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                required
                                name='password'
                                type={values.showPassword ? 'text' : 'password'}
                                value={input.password}
                                onChange={handleChangeFormUser}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={clickShowPassword}
                                    onMouseDown={mouseDownPassword}
                                    edge="end"
                                    >
                                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                                }
                                labelWidth={80}
                            />
                        </FormControl>
                        <br/>
                        <Button style={{marginTop:'10px',marginLeft:'140px'}} variant='outlined' color='primary' onClick={handleSubmitFormUser} onSubmit={handleSubmitFormUser}>Register</Button>
                    </form>
                </div>
            </>
        )
    }

export default Register