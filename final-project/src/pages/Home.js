import React, { Component } from 'react'
import Axios from 'axios'

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

function minuteToHours(num){
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return ( rhours === 0 ? "" : rhours + " Jam") + (rminutes === 0 ? "" : " " + rminutes + " Menit")
};

function turncateString (str,num) {
    if(str===null){
        return '';
    } else {
        if(str.length<=num){
            return str;
        } else{
            return str.slice(0,num)+'...';
        }
    }
}

class Home extends Component{
    constructor(props){
        super(props)
        this.state={
            movies:[],
            games:[]
        }
    }

    componentDidMount(){
        Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res=>{
            let movies = res.data.map(el=>{ return {
                id: el.id, 
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url
            }})
            this.setState({movies:movies})         
        })
        Axios.get('https://backendexample.sanbersy.com/api/data-game')
        .then(res=>{
            let games = res.data.map(el=>{ return {
                id: el.id, 
                name: el.name, 
                platform: el.platform,
                release: el.release,
                genre: el.genre,
                singlePlayer: el.singlePlayer,
                multiplayer:el.multiplayer,
                image_url: el.image_url
            }})
            this.setState({games:games})
        })
    }

    render(){
        return(
            <>
                <div style={{marginTop:'120px', marginLeft:'20px'}}>
                    <h1>Movies</h1>
                    <div style={{display:'flex'}}>
                        {this.state.movies!== null && this.state.movies.map((item)=>{
                            return(
                                <>
                                    <div style={{marginLeft:'20px'}}>
                                        <Card style={{width: 345}}>
                                            <CardHeader
                                                title={item.title}
                                                subheader={`Year ${item.year}`}
                                            />
                                            <CardMedia
                                                style={{height: 0, paddingTop: '56.25%'}}
                                                image={item.image_url}
                                            />
                                            <CardContent>
                                                <Typography variant="body2" color="textSecondary" component="p">
                                                    Durasi:{minuteToHours(item.duration)}<br/>
                                                    Genre: {item.genre}<br/>
                                                    Rating: {item.rating} <br/>
                                                    Synopsis:<br/>{turncateString(item.description,200)}
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </div>
                                </>
                            )
                        })}
                    </div>
                    <h1>Games</h1>
                    <div style={{display:'flex'}}>
                        {this.state.games!== null && this.state.games.map((item)=>{
                            return(
                                <>
                                    <div style={{marginLeft:'20px'}}>
                                        <Card style={{width: 345}}>
                                            <CardHeader
                                                title={item.name}
                                                subheader={`Year ${item.release}`}
                                            />
                                            <CardMedia
                                                style={{height:0,paddingTop:'56.25%'}}
                                                image={item.image_url}
                                            />
                                            <CardContent>
                                                <Typography variant="body2" color="textSecondary" component="p">
                                                    Platform:{item.platform}<br/>
                                                    Genre: {item.genre}<br/>
                                                    Single Player: {item.singlePlayer?'Yes':'No'} <br/>
                                                    Multi Player:{item.multiplayer?'Yes':'No'}
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </div>
                                </>
                            )
                        })}
                    </div>
                </div>
            </>
        )
    }

}

export default Home