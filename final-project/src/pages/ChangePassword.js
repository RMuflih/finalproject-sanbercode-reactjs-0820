import React, {useState,useContext} from 'react'
import Axios from 'axios'

import {UserContext} from '../context/UserContext'

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {Button} from '@material-ui/core'

const ChangePassword =()=>{
    const [user, setUser] = useContext(UserContext)
    const [input, setInput] = useState({password: "", newPassword: "" , confirmPassword: ""})

    const useStyles = makeStyles((theme) => ({
        margin: {
          marginLeft:'25%',
          marginTop: '10px'
        },
        textField: {
          width: '25ch',
        },
      }));
    
    const classes = useStyles();
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleSubmitFormUser = (event) =>{
        event.preventDefault()
        Axios.post("https://backendexample.sanbersy.com/api/change-password", {
        current_password: input.password, 
        new_password: input.newPassword, 
        new_confirm_password: input.confirm
        },{headers:{"Authorization":`Bearer ${user.token}`}})
        .then((res)=>{
            var user = res.data.user
            var token = res.data.token
            var currentUser = {current_password: user.current_password, new_password: user.new_password,new_confirm_password:user.new_confirm_password, token }
            setUser(currentUser)
            localStorage.setItem("user", JSON.stringify(currentUser))
        }
        ).catch((err)=>{
            alert('Change Password Success')
            setInput({password: "", newPassword: "" , confirmPassword: ""})
        })
    }

    const handleChangeFormUser = (event) =>{
        let value = event.target.value
        let name = event.target.name
        switch (name){
        case "password":{
            setInput({...input, password: value})
            break;
        }
        case "newPassword":{
            setInput({...input, newPassword: value})
            break;
        }
        case "confirmPassword":{
            setInput({...input, confirmPassword: value})
            break;
        }
        default:{break;}
        }
    }

    const clickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    
    const mouseDownPassword = (event) => {
        event.preventDefault();
    };

    return(
        <>
            <div style={{margin: "100px auto", width: "25%", padding: "50px"}}>
                <form onSubmit={handleSubmitFormUser}>
                    <label style={{fontSize:'8pt',marginLeft:'80px'}}>Current Password:</label><br/>
                    <FormControl className={clsx(classes.margin, classes.textField)} style={{marginBottom:'20px'}} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password"> Current Password *</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            required
                            name='password'
                            type={values.showPassword ? 'text' : 'password'}
                            value={input.password}
                            onChange={handleChangeFormUser}
                            endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                aria-label="toggle password visibility"
                                onClick={clickShowPassword}
                                onMouseDown={mouseDownPassword}
                                edge="end"
                                >
                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                            }
                        labelWidth={140}
                        />
                    </FormControl>
                    <br/>
                    <label style={{fontSize:'8pt',marginLeft:'80px'}}>New Password:</label><br/>
                    <FormControl className={clsx(classes.margin, classes.textField)} style={{marginBottom:'20px'}} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password"> New Password *</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            required
                            name="newPassword"
                            type={values.showPassword ? 'text' : 'password'}
                            value={input.newPassword}
                            onChange={handleChangeFormUser}
                            endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                aria-label="toggle password visibility"
                                onClick={clickShowPassword}
                                onMouseDown={mouseDownPassword}
                                edge="end"
                                >
                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                            }
                        labelWidth={120}
                        />
                    </FormControl>
                    <label style={{fontSize:'8pt',marginLeft:'80px'}}>Confirm Password:</label><br/>
                    <FormControl className={clsx(classes.margin, classes.textField)} style={{marginBottom:'20px'}} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password"> Confirm Password *</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            required
                            name="confirmPassword"
                            type={values.showPassword ? 'text' : 'password'}
                            value={input.confirmPassword}
                            onChange={handleChangeFormUser}
                            endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                aria-label="toggle password visibility"
                                onClick={clickShowPassword}
                                onMouseDown={mouseDownPassword}
                                edge="end"
                                >
                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                            }
                        labelWidth={150}
                        />
                    </FormControl>
                    <br/>
                    <Button style={{marginTop:'10px',marginLeft:'100px'}} variant='outlined' color='primary' onClick={handleSubmitFormUser} onSubmit={handleSubmitFormUser}>Change Password</Button>
                </form>
            </div>
        </>
    )
}

export default ChangePassword