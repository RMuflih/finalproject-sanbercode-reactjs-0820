import React,{useState,useContext} from 'react'
import Axios from 'axios'
import {Link,Switch,Route} from 'react-router-dom'

import {GamesContext} from '../../context/GamesContext'
import {UserContext} from '../../context/UserContext'
import GamesForm from './GamesForm'

import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}
  
function getComparator(order, orderBy) {
    return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}
  
const headCells = [
    { id: 'name', label: 'Name'},
    { id: 'platform', label: 'Platform' },
    { id: 'release', label: 'Release' },
    { id: 'genre', label: 'Genre' },
];
  
function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
    };
    
    return (
    <TableHead>
        <TableRow>
        <TableCell style={{marginLeft:'20px'}}>No</TableCell>
        {headCells.map((headCell) => (
            <TableCell
            key={headCell.id}
            align="center"
            sortDirection={orderBy === headCell.id ? order : false}
            >
                <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
                >
                    {headCell.label}
                    {(orderBy === headCell.id) ? (
                        <span className={classes.visuallyHidden}>
                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </span>
                    ) : null}
                </TableSortLabel>
            </TableCell>
        ))}
        <TableCell style={{marginLeft:'20px'}} align="center">Single Player</TableCell>
        <TableCell style={{marginLeft:'20px'}} align="center">Multi Player</TableCell>
        <TableCell style={{marginLeft:'20px'}} align="center">Action</TableCell>
        </TableRow>
    </TableHead>
    );
}
  
EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

const Games = () => {
    const [user] = useContext(UserContext)
    const [games,setGames,,setInputGame] = useContext(GamesContext)
    const classes = useStyles()
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('name');
    const [search,setSearch] = useState('')

    const handleEditFormGame = (event) => {
        let itemId= parseInt(event.target.value)
        let singleGame=games.find(x=>x.id===itemId)
        setInputGame({
            id:itemId,
            name: singleGame.name,
            platform: singleGame.platform,
            release: singleGame.release,
            genre: singleGame.genre,
            singlePlayer: singleGame.singlePlayer,
            multiPlayer: singleGame.multiPlayer,
            image_url: singleGame.image_url
        })
        console.log(games)
    }

    const handleDeleteFormGame = (event) => {
        var idGame=parseInt(event.target.value)
        Axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`,{headers: {"Authorization" : `Bearer ${user.token}`}})
        .then(res=>{
            var newGame=games.filter(x=>x.id!==idGame)
            setGames(newGame)
        })
    }

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const turncateString = (str,num) =>{
        if(str===null){
            return ''
        } else {
            if(str.length<=num){
                return str
            } else{
                return str.slice(0,num)+'...'
            }
        }
    }

    const handleSubmitSearch = (e)=>{
        e.preventDefault()
        Axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let resGames = res.data.map(el=>{ return {
                    id: el.id, 
                    name: el.name, 
                    platform: el.platform,
                    release: el.release,
                    genre: el.genre,
                    singlePlayer: el.singlePlayer,
                    multiPlayer: el.multiPlayer,
                    image_url: el.image_url
                }
            })
            let filteredGames = resGames.filter(x=> x.name.toLowerCase().indexOf(search.toLowerCase()) !== -1)
            setGames([...filteredGames])
        })
    }

    const handleChangeSearch = (event)=>{
        let value = event.target.value
        setSearch(value)
    }

    return(
        <>
            <div style={{marginTop:'100px'}}>
                <form onSubmit={handleSubmitSearch} style={{marginLeft:'600px'}}>
                    <Autocomplete
                        freeSolo
                        id="free-solo-2-demo"
                        disableClearable
                        value={search}
                        onChange={handleChangeSearch}
                        options={games!==null && games.map((option) => option.name)}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label="Search"
                                margin="normal"
                                variant="outlined"
                                value={search}
                                onChange={handleChangeSearch}
                                style={{width:'400px',zIndex:'0'}}
                                InputProps={{ ...params.InputProps, type: 'search' }}
                            />
                        )}
                    />
                </form>
                <TableContainer component={Paper}>
                    <Table 
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                        <TableBody>
                            {games!==null && stableSort(games, getComparator(order, orderBy))
                            .map((item, index) => {

                                return (
                                    <TableRow
                                    hover
                                    role="checkbox"
                                    tabIndex={-1}
                                    key={item.id}
                                    >
                                        <TableCell>{index+1}</TableCell>
                                        <TableCell align="center">{item.name}</TableCell>
                                        <TableCell align="center" title={item.platform}>{turncateString(item.platform,20)}</TableCell>
                                        <TableCell align="center">{item.release}</TableCell>
                                        <TableCell align="center" title={item.genre}>{turncateString(item.genre,25)}</TableCell>
                                        <TableCell align="center">{item.singlePlayer?'Yes':'No'}</TableCell>
                                        <TableCell align="center">{item.multiplayer?'Yes':'No'}</TableCell>
                                        <TableCell align="center">
                                            <Link to='/GamesForm'><button value={item.id} onClick = {handleEditFormGame}>Edit</button></Link>
                                            <button value={item.id} onClick = {handleDeleteFormGame}>Delete</button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Switch>
                    <Route exact path='/GamesForm' component={GamesForm}/>
                </Switch>
            </div>
        </>
    )
}

export default Games