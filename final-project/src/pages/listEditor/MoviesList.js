import Axios from 'axios'
import React,{useContext,useState} from 'react'
import {Link, Switch, Route} from 'react-router-dom'

import {MoviesContext} from '../../context/MoviesContext'
import {UserContext} from '../../context/UserContext'
import MoviesForm from './MoviesForm'

import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}
  
function getComparator(order, orderBy) {
    return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}
  
const headCells = [
    { id: 'title', label: 'Title'},
    { id: 'description', label: 'Description'},
    { id: 'year', label: 'Year' },
    { id: 'duration', label: 'Duration' },
    { id: 'genre', label: 'Genre'},
    { id: 'rating', label: 'Rating'},
];
  
function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
    };
    
    return (
    <TableHead>
        <TableRow>
        <TableCell style={{marginLeft:'20px'}}>No</TableCell>
        {headCells.map((headCell) => (
            <TableCell
            key={headCell.id}
            align="center"
            sortDirection={orderBy === headCell.id ? order : false}
            >
                <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
                >
                    {headCell.label}
                    {(orderBy === headCell.id) ? (
                        <span className={classes.visuallyHidden}>
                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </span>
                    ) : null}
                </TableSortLabel>
            </TableCell>
        ))}
        <TableCell style={{marginLeft:'20px'}}>Action</TableCell>
        </TableRow>
    </TableHead>
    );
}
  
EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 100,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

const Movies = () => {
    const [user]=useContext(UserContext)
    const [movies,setMovies,,setInputMovie] = useContext(MoviesContext)
    const classes = useStyles()
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('title');
    const [search,setSearch] = useState('')

    const handleEditFormMovie = (event) => {
        let itemId= parseInt(event.target.value)
        let singleMovie=movies.find(x=>x.id===itemId)
        setInputMovie({
            id:itemId,
            title: singleMovie.title,
            description: singleMovie.description,
            year: singleMovie.year,
            duration: singleMovie.duration,
            genre: singleMovie.genre,
            rating: singleMovie.rating,
            image_url: singleMovie.image_url
        })
    }

    const handleDeleteFormMovie = (event) => {
        var idMovie=parseInt(event.target.value)
        Axios.delete( `https://backendexample.sanbersy.com/api/data-movie/${idMovie}`,{headers: {"Authorization" : `Bearer ${user.token}`}})
        .then(res=>{
            var newMovie=movies.filter(x=>x.id!==idMovie)
            setMovies(newMovie)
        })
    }

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    const turncateString = (str,num) =>{
        if(str===null){
            return ''
        } else {
            if(str.length<=num){
                return str
            } else{
                return str.slice(0,num)+'...'
            }
        }
    }

    const handleSubmitSearch = (e)=>{
        e.preventDefault()
        Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let resMovies = res.data.map(el=>{ return {
                    id: el.id, 
                    title: el.title, 
                    description: el.description,
                    year: el.year,
                    duration: el.duration,
                    genre: el.genre,
                    rating: el.rating,
                    image_url: el.image_url
                }
            })
            let filteredMovies = resMovies.filter(x=> x.title.toLowerCase().indexOf(search.toLowerCase()) !== -1)
            setMovies([...filteredMovies])
        })
    }

    const handleChangeSearch = (event)=>{
        let value = event.target.value
        setSearch(value)
    }

    return(
        <>
            <div style={{marginTop:'100px'}}>
                <form onSubmit={handleSubmitSearch} style={{marginLeft:'600px'}}>
                    <Autocomplete
                        freeSolo
                        id="free-solo-2-demo"
                        disableClearable
                        value={search}
                        onChange={handleChangeSearch}
                        options={movies!==null && movies.map((option) => option.title)}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label="Search"
                                margin="normal"
                                variant="outlined"
                                value={search}
                                onChange={handleChangeSearch}
                                style={{width:'400px',zIndex:'0'}}
                                InputProps={{ ...params.InputProps, type: 'search' }}
                            />
                        )}
                    />
                </form>
                <TableContainer component={Paper}>
                    <Table 
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                        <TableBody>
                            {movies!==null && stableSort(movies, getComparator(order, orderBy))
                            .map((item, index) => {

                                return (
                                    <TableRow
                                    hover
                                    role="checkbox"
                                    tabIndex={-1}
                                    key={item.id}
                                    >
                                        <TableCell>{index+1}</TableCell>
                                        <TableCell align="center">{item.title}</TableCell>
                                        <TableCell align="center" title={item.description}>{turncateString(item.description,20)}</TableCell>
                                        <TableCell align="center">{item.year}</TableCell>
                                        <TableCell align="center" >{item.duration}</TableCell>
                                        <TableCell align="center">{item.genre}</TableCell>
                                        <TableCell align="center">{item.rating}</TableCell>
                                        <TableCell align="center">
                                            <Link to='/MoviesForm'><button value={item.id} onClick = {handleEditFormMovie}>Edit</button></Link>
                                            <button value={item.id} onClick = {handleDeleteFormMovie}>Delete</button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Switch>
                    <Route exact path='/MoviesForm' component={MoviesForm}/>
                </Switch>
            </div>
        </>
    )
}

export default Movies