import React from 'react'
import {GamesProvider} from '../../context/GamesContext'
import SideBarGame from '../../layouts/SidebarGame'

const Games = () => {
    return(
        <>  
            <GamesProvider>
                <div style={{display:'flex',marginRight:'20px'}}>
                    <SideBarGame/>
                </div>
            </GamesProvider>
        </>
    )
}

export default Games