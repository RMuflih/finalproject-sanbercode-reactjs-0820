import React, {useContext} from 'react'
import Axios from 'axios'

import {MoviesContext} from '../../context/MoviesContext'
import {UserContext} from '../../context/UserContext'

const MoviesForm = () => {
    const [user]=useContext(UserContext)
    const [movies,setMovies,inputMovie,setInputMovie] = useContext(MoviesContext)

    const handleSubmitFormMovie = (event) => {
        event.preventDefault()
        if(inputMovie.id===null){
            Axios.post('https://backendexample.sanbersy.com/api/data-movie',{title:inputMovie.title,
            description:inputMovie.description,year:inputMovie.year,duration:inputMovie.duration,genre:inputMovie.genre,
            rating:inputMovie.rating,image_url:inputMovie.image_url},{headers:{"Authorization":`Bearer ${user.token}`}})
            .then(res=>{
                var data=res.data

                setMovies([...movies,{id:data.id,title:data.title,description:data.description,
                year:data.year,duration:data.duration,genre:data.genre,rating:data.rating,
                image_url:data.image_url}])

                setInputMovie({id:null,title:"",description:"",
                year:2020,duration:120,genre:"",rating:0,
                image_url:""})

                alert("input Success")
            })
        } else {
            Axios.put(`https://backendexample.sanbersy.com/api/data-movie/${inputMovie.id}`,
            {title:inputMovie.title,description:inputMovie.description,year:inputMovie.year,duration:inputMovie.duration,genre:inputMovie.genre,
            rating:inputMovie.rating,image_url:inputMovie.image_url},{headers: {"Authorization" : `Bearer ${user.token}`}})
            .then(res=>{
                var singleMovie=movies.map(x=>{
                    if(x.id===inputMovie.id){
                        x.title = inputMovie.title
                        x.description = inputMovie.description
                        x.year = inputMovie.year
                        x.duration = inputMovie.duration
                        x.genre = inputMovie.genre
                        x.rating = inputMovie.rating
                        x.image_url = inputMovie.image_url
                    }
                    return x
                })
                setMovies(singleMovie)
                setInputMovie({
                    id:null,
                    title:'',
                    description:'',
                    year: 2020,
                    duration: 120,
                    genre:'',
                    rating: 0,
                    image_url:''
                })
            })
        }
    }

    const handleChangeFormMovie = (event) => {
        let typeOfInput = event.target.name
        switch (typeOfInput) {
            case "title":
            {
                setInputMovie({...inputMovie, title: event.target.value});
                break
            }
            case "description":
            {
                setInputMovie({...inputMovie, description: event.target.value});
                break
            }
            case "year":
            {
                setInputMovie({...inputMovie, year: event.target.value});
                break
            }
            case "duration":
            {
                setInputMovie({...inputMovie, duration: event.target.value});
                break
            }
            case "genre":
            {
                setInputMovie({...inputMovie, genre: event.target.value});
                break
            }
          case "rating":
            {
                setInputMovie({...inputMovie, rating: event.target.value});
                break
            }
          case "image_url":
            {
                setInputMovie({...inputMovie, image_url: event.target.value});
                break
            }
            default:
                {break;}
        }
    }

    return(
        <>
            <div style={{marginTop:'100px'}}>
                <form onSubmit={handleSubmitFormMovie}>
                    <table style={{border:'none'}}>
                        <tr>
                            <th>Title:</th>
                            <td><input required type="text" value={inputMovie.title} name='title' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Description:</th>
                            <td><textarea required value={inputMovie.description} name='description' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Year:</th>
                            <td><input required type="number" value={inputMovie.year} name='year' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Duration:</th>
                            <td><input required type="number" value={inputMovie.duration} name='duration' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Genre:</th>
                            <td><input required type="text" value={inputMovie.genre} name='genre' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Rating:</th>
                            <td><input required type="number" value={inputMovie.rating} name='rating' onChange={handleChangeFormMovie}/></td>
                        </tr>
                        <tr>
                            <th>Image Url:</th>
                            <td><textarea value={inputMovie.image_url} name='image_url' onChange={handleChangeFormMovie}/></td>
                        </tr>
                    </table>
                    <button style={{marginLeft:'40px',marginTop:'50px'}}>submit</button>
                </form>
            </div>
        </>
    )
}

export default MoviesForm