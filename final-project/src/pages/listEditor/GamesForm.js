import React, {useContext} from 'react'
import Axios from 'axios'

import {GamesContext} from '../../context/GamesContext'
import {UserContext} from '../../context/UserContext'

const Games = () =>{
    const [user] = useContext(UserContext)
    const [games,setGames,inputGame,setInputGame] = useContext(GamesContext)

    const handleSubmitFormGame = (event) => {
        event.preventDefault()
        if(inputGame.id===null){
            Axios.post(`https://backendexample.sanbersy.com/api/data-game`,{name: inputGame.name,
            platform: inputGame.platform, release: inputGame.release, genre: inputGame.genre,
            singlePlayer: inputGame.singlePlayer, multiplayer: inputGame.multiplayer,image_url:inputGame.image_url},{headers:{"Authorization":`Bearer ${user.token}`}})
            .then(res=>{
                var data=res.data

                setGames([...games,{id:data.id,name:data.name,platform:data.platform,
                release:data.release,genre:data.genre,singlePlayer:data.singlePlayer,multiplayer:data.multiplayer,
                image_url:data.image_url}])

                setInputGame({id:null,name:"",platform:"",
                release:2020,genre:"",singlePlayer:false,multiplayer:false,
                image_url:""})

                alert('Input Success')
            })
        } else {
            Axios.put(`https://backendexample.sanbersy.com/api/data-game/${inputGame.id}`,{name: inputGame.name,
            platform: inputGame.platform, release: inputGame.release, genre: inputGame.genre,
            singlePlayer: inputGame.singlePlayer, multiplayer: inputGame.multiplayer,image_url:inputGame.image_url},{headers:{"Authorization":`Bearer ${user.token}`}})
            .then(res=>{
                var singleGame=games.map(x=>{
                    if(x.id===inputGame.id){
                        x.name = inputGame.name
                        x.platform = inputGame.platform
                        x.release = inputGame.release
                        x.genre = inputGame.genre
                        x.singlePlayer = inputGame.singlePlayer
                        x.multiplayer = inputGame.multiplayer
                        x.image_url = inputGame.image_url
                    }
                    return x
                })
                setGames(singleGame)
                setInputGame({
                    id:null,
                    name:'',
                    platform:'',
                    release: 2020,
                    genre: '',
                    singlePlayer:false,
                    multiplayer: false,
                    image_url:''
                })
            })
        }
    }

    const handleChangeFormGame = (event) => {
        let typeOfInput = event.target.name
        let value = event.target.value
        switch (typeOfInput) {
            case 'name':{
                setInputGame({...inputGame,name:value})
                break;
            }
            case 'platform':{
                setInputGame({...inputGame,platform:value})
                break;
            }
            case 'release':{
                setInputGame({...inputGame,release:value})
                break;
            }
            case 'genre':{
                setInputGame({...inputGame,genre:value})
                break;
            }
            case 'singlePlayer':{
                setInputGame({...inputGame,singlePlayer:value})
                break;
            }
            case 'multiPlayer':{
                setInputGame({...inputGame,multiplayer:value})
                break;
            }
            case 'image_url':{
                setInputGame({...inputGame,image_url:value})
                break;
            }
            default: {break;}
        }
    }

    return(
        <>
            <div style={{marginTop:'100px'}}>
                <form onSubmit={handleSubmitFormGame}>
                    <table style={{border:'none'}}>
                        <tr>
                            <th>Name:</th>
                            <td><input required type="text" value={inputGame.name} name='name' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Platform:</th>
                            <td><input required type='text' value={inputGame.platform} name='platform' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Release:</th>
                            <td><input required type="number" value={inputGame.release} name='release' min='2000' max='2025' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Genre:</th>
                            <td><input required type="text" value={inputGame.genre} name='genre' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Single Player:</th>
                            <td><input required type="number" value={inputGame.singlePlayer?1:0} name='singlePlayer' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Multi Player:</th>
                            <td><input required type="number" value={inputGame.multiplayer?1:0} name='multiPlayer' onChange = {handleChangeFormGame}/></td>
                        </tr>
                        <tr>
                            <th>Image Url:</th>
                            <td><textarea required value={inputGame.image_url} name='image_url' onChange = {handleChangeFormGame}/></td>
                        </tr>
                    </table>
                    <button style={{marginLeft:'40px',marginTop:'10px'}}>submit</button>
                </form>
            </div>  
        </>
    )
}

export default Games