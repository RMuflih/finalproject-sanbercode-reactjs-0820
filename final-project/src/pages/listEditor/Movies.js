import React from 'react'

import {MoviesProvider} from '../../context/MoviesContext'
import SideBarMovie from '../../layouts/SidebarMovie'

const Movies = () =>{
    return(
        <>
            <MoviesProvider>
                <div style={{display:'flex'}}>
                    <SideBarMovie/>
                </div>
            </MoviesProvider>
        </>
    )
}

export default Movies