import React, { useContext, useState } from "react"
import {UserContext} from "../context/UserContext"
import axios from "axios"

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {Button} from '@material-ui/core'

const Login = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({email: "" , password: ""})

  const useStyles = makeStyles((theme) => ({
    margin: {
      marginLeft:'25%',
      marginTop: '10px'
    },
    textField: {
      width: '25ch',
    },
  }));

  const classes = useStyles();
  const [values, setValues] = React.useState({
      amount: '',
      password: '',
      weight: '',
      weightRange: '',
      showPassword: false,
  });

  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post("https://backendexample.sanbersy.com/api/user-login", {
      email: input.email, 
      password: input.password
    }).then(
      (res)=>{
        var user = res.data.user
        var token = res.data.token
        var currentUser = {name: user.name, email: user.email, token }
        setUser(currentUser)
        localStorage.setItem("user", JSON.stringify(currentUser))
        alert("Login Success")
      }
    ).catch((err)=>{
      alert(err)
    })
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  const clickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const mouseDownPassword = (event) => {
    event.preventDefault();
  };

  return(
    <>
      <div style={{margin: "100px auto", width: "25%", padding: "50px"}}>
        <form onSubmit={handleSubmit}>
          <label style={{fontSize:'8pt',marginLeft:'80px'}}>Masukkan Email Anda: </label><br/>
          <TextField className={clsx(classes.margin, classes.textField)} required name="email" id="outlined-required" label="Name" onChange={handleChange} style={{marginBottom:'20px'}} value={input.email} variant="outlined"/>
          {/* <label>Email: </label>
          <input type="email" name="email" onChange={handleChange} value={input.email}/> */}
          <br/>
          <label style={{fontSize:'8pt',marginLeft:'80px'}}>Masukkan Password Anda:</label><br/>
          <FormControl className={clsx(classes.margin, classes.textField)} style={{marginBottom:'20px'}} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">Password *</InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              require
              name='password'
              type={values.showPassword ? 'text' : 'password'}
              value={input.password}
              onChange={handleChange}
              endAdornment={
              <InputAdornment position="end">
                <IconButton
                aria-label="toggle password visibility"
                onClick={clickShowPassword}
                onMouseDown={mouseDownPassword}
                edge="end"
                >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
              }
              labelWidth={80}
            />
          </FormControl>
          {/* <label>Password: </label>
          <input type="password" name="password" onChange={handleChange} value={input.password}/> */}
          <br/>
          <Button style={{marginTop:'10px',marginLeft:'140px'}} variant='outlined' color='primary' onClick={handleSubmit}>Login</Button>
        </form>
      </div>
    </>
  )
}

export default Login