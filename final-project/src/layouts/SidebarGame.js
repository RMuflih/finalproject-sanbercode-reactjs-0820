import React from 'react'
import {Link,Switch,Route,BrowserRouter as Router} from 'react-router-dom'

import GamesList from '../pages/listEditor/GamesList'
import GamesForm from '../pages/listEditor/GamesForm'

const SideBarGame = () =>{
    return(
        <>  
            <Router>
                <div style={{
                    marginTop:'100px',
                    backgroundColor:'#aaa',
                    marginLeft:'20px',
                    width:"200px",
                    textAlign:'center',
                    height:'300px',
                    marginRight:'20px'
                }}>
                    <Link style={{textDecoration:'none'}} to='/GamesList'>List</Link>
                    <hr/>
                    <Link style={{textDecoration:'none'}} to='/GamesForm'>Form</Link>
                </div>
                <Switch>
                    <Route exact path='/GamesList'>
                        <GamesList/>
                    </Route>
                    <Route exact path='/GamesForm'>
                        <GamesForm/>
                    </Route>

                </Switch>
            </Router>
        </>
    )
}

export default SideBarGame