import React from 'react'
import {Link,Switch,Route,BrowserRouter as Router} from 'react-router-dom'

import MoviesList from '../pages/listEditor/MoviesList'
import MoviesForm from '../pages/listEditor/MoviesForm'

const SideBarMovie = () =>{
    return(
        <>
            <Router>
                <div style={{
                    marginTop:'100px',
                    backgroundColor:'#aaa',
                    marginLeft:'20px',
                    width:"200px",
                    textAlign:'center',
                    height:'300px',
                    marginRight:'20px'
                }}>
                    <Link style={{textDecoration:'none'}} to='/MoviesList'>List</Link>
                    <hr/>
                    <Link style={{textDecoration:'none'}} to='/MoviesForm'>Form</Link>
                </div>
                <Switch>
                    <Route exact path='/MoviesList'>
                        <MoviesList/>
                    </Route>
                    <Route exact path='/MoviesForm'>
                        <MoviesForm/>
                    </Route>
                </Switch>
            </Router>
        </>
    )
}

export default SideBarMovie