import React,{useContext} from 'react'
import {
    Switch,
    Route,
    Redirect
} from 'react-router-dom'

import Home from '../pages/Home'
import Movies from '../pages/listEditor/Movies'
import Games from '../pages/listEditor/Games'
import Register from '../pages/Register'
import Login from '../pages/Login'
import ChangePassword from '../pages/ChangePassword'
import {UserContext} from '../context/UserContext'

const Section = () =>{
    const [user] = useContext(UserContext);

    const PrivateRoute = ({user, ...props }) => {
        if (user) {
          return <Route {...props} />;
        } else {
          return <Redirect to="/Login" />;
        }
    };

    const LoginRoute = ({user, ...props }) =>
    user ? <Redirect to="/" /> : <Route {...props} />;  

    return(
        <section>
            <Switch>
                <Route exact path='/' user={user} component={Home}/>
                <PrivateRoute exact path='/MoviesList' user={user} component={Movies}/>
                <PrivateRoute exact path='/GamesList' user={user} component={Games}/>
                <PrivateRoute exact path='/ChangePassword' user={user} component={ChangePassword}/>
                <LoginRoute path='/Register' user={user} component={Register}/>
                <LoginRoute path='/Login' user={user} component={Login}/>
            </Switch>
        </section>
    )
}

export default Section