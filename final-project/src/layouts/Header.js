import React, {useContext} from 'react'
import {Button} from '@material-ui/core'
import {Link} from 'react-router-dom'
import './layout.css'

import {UserContext} from '../context/UserContext'

const Header=()=>{
    const [user,setUser] = useContext(UserContext)

    const Logout = () => {
        setUser(null)
        localStorage.removeItem('user')
        localStorage.clear()
    }

    return(
        <header>
            <Link to='/'>
                <img src='/img/logo.png' alt='' width='200px'/>
            </Link>
            <nav>
                <ul>
                    <li className={'kanan'}>
                        <Link to='/'>
                            <Button variant='outlined' color='primary'>
                                Home
                            </Button>
                        </Link>
                    </li>
                    {user &&
                        <>
                            <li className={'kanan'}>
                                <Link to='/MoviesList'>
                                    <Button variant='outlined' color='primary'>
                                        Movie List Editor
                                    </Button>
                                </Link>
                            </li>
                            <li className={'kanan'}>
                                <Link to='/GamesList'>
                                    <Button variant='outlined' color='primary'>
                                        Game List Editor
                                    </Button>
                                </Link>
                            </li>
                            <li className={'changePassword'}>
                                <Link to='/ChangePassword'>
                                    <Button variant='outlined' color='primary'>
                                        Change Password
                                    </Button>
                                </Link>
                            </li>
                            <li className={'logout'}>
                                <Link onClick={Logout}>
                                    <Button variant='outlined' color='primary'>
                                        Logout
                                    </Button>
                                </Link>
                            </li>
                        </>
                    }
                    {user === null && 
                        <>
                            <li className={'log-reg'}>
                                <Link to='/Register'>
                                    <Button variant='outlined' color='primary'>
                                        Register
                                    </Button>
                                </Link>
                            </li>
                            <li style={{marginLeft:'20px'}}>
                                <Link to='/Login'>
                                    <Button variant='outlined' color='primary'>
                                        Login
                                    </Button>
                                </Link>
                            </li>
                        </>
                    }
                </ul>
            </nav>
        </header>
    )
}

export default Header