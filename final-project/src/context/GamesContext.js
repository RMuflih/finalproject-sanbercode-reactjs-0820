import React, {useState,createContext, useEffect} from 'react'
import Axios from 'axios'

export const GamesContext = createContext()

export const GamesProvider = props =>{
    const [games,setGames]=useState(null)
    const [inputGame,setInputGame] = useState({
        id:null,
        name:'',
        platform:'',
        release:2020,
        genre:'',
        singlePlayer:false,
        multiplayer:false,
        image_url:''
    })
    useEffect(()=>{
        if(games===null){
            Axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            .then(res=>{
                setGames(res.data)
            })
        }
    },[games])

    return(
        <GamesContext.Provider value={[games,setGames,inputGame,setInputGame]}>
            {props.children}
        </GamesContext.Provider>
    )
}