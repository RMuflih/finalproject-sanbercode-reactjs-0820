import React, {useState,createContext, useEffect} from 'react'
import Axios from 'axios'

export const MoviesContext = createContext()

export const MoviesProvider = props =>{
    const [movies,setMovies]=useState(null)
    const [inputMovie,setInputMovie] = useState({
        id:null,
        title:'',
        description:'',
        year: 2020,
        duration: 120,
        genre:'',
        rating: 0,
        image_url:''
    })
    useEffect(()=>{
        if(movies===null){
            Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            .then(res=>{
                setMovies(res.data)
            })
        }
    },[movies])

    return(
        <MoviesContext.Provider value={[movies,setMovies,inputMovie,setInputMovie]}>
            {props.children}
        </MoviesContext.Provider>
    )
}